# Análisis Avanzado

Primer Cuatrimestre 2024 \
Universidad de Buenos Aires

## Prácticas

| Nro |      Titulo      | Enunciado                                                                                          | Solución                                                                                                      |
|-----|------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1   | Supremos, ínfimos y sucesiones | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica1.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica1/practica1.pdf)
| 2   | Cardinalidad | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica2.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica2/practica2.pdf)
| 3   | Espacios métricos | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica3.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica3/practica3.pdf)
| 4   | Funciones continuas | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica4.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica4/practica4.pdf)
| 5   | Compacidad | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica5.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica5/practica5.pdf)
| 6   | Espacios normados | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica6.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica6/practica6.pdf)
| 7   | Sucesiones y series de funciones | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica7.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica7/practica7.pdf)
| 8   | Medida de Lebesgue | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica8.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica8/practica8.pdf)
| 9   | Funciones medibles e integral de Lebesgue | [Enunciado](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/enunciados/practica9.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/practicas/soluciones/practica9/practica9.pdf)

## Parciales 
| Nro |      Titulo      | Solución                                                                                          |
|-----|------------------|----------------------------------------------------------------------------------------------------|
| 1   | Primer Parcial | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/parciales/1er%20parcial%20An%C3%A1lisis%20Avanzado12024.soluciones.pdf)
| 2   | Segundo Parcial | [Solución](https://gitlab.com/faustomartinez/uba-analisis-avanzado/-/blob/master/parciales/2do%20parcial%20An%C3%A1lisis%20Avanzado.soluciones.pdf)